//
//  main.swift
//  Sandbox
//
//  Created by robyandell on 21/10/2018.
//  Copyright © 2018 Rob Yandell. All rights reserved.
//

import Foundation

struct MySettings: Codable {
    var someFlag: Bool
    var someString: String
    var someInt: Int
}

let settingsURL = URL (fileURLWithPath: "/Volumes/Code Library/xcode/Sandbox/Sandbox/info.plist")
var settings: MySettings?

if let data = try? Data(contentsOf: settingsURL) {
    let decoder = PropertyListDecoder()
    settings = try? decoder.decode(MySettings.self, from: data)
}

print(settings)
